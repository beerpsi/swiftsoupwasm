FROM debian:unstable-slim

LABEL maintainer="beerpsi <beerpsi@duck.com>"
LABEL Description="Docker container for SwiftWasm, Binaryen, WABT and make"
LABEL org.opencontainers.image.source https://gitlab.com/beerpsi/swiftsoupwasm

ENV DEBIAN_FRONTEND=noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN=true

RUN apt-get update && \
    apt-get install -y --no-install-recommends ca-certificates curl

ARG SWIFT_TAG=swift-wasm-5.6.0-RELEASE
ENV SWIFT_TAG=$SWIFT_TAG
RUN curl -L "https://github.com/swiftwasm/swift/releases/download/$SWIFT_TAG/$SWIFT_TAG-ubuntu20.04_x86_64.tar.gz" | \
        tar -xzf - --directory / --strip-components=1 && \
        chmod -R o+r /usr/lib/swift

ARG WABT_TAG=1.0.29
ENV WABT_TAG=$WABT_TAG
RUN curl -L "https://github.com/WebAssembly/wabt/releases/download/$WABT_TAG/wabt-$WABT_TAG-ubuntu.tar.gz" | \
        tar -xzf - --directory /usr --strip-components=1

ARG BINARYEN_TAG=version_109
ENV BINARYEN_TAG=$BINARYEN_TAG
RUN curl -L "https://github.com/WebAssembly/binaryen/releases/download/$BINARYEN_TAG/binaryen-$BINARYEN_TAG-x86_64-linux.tar.gz" | \
        tar -xzf - --directory /usr --strip-components=1

RUN export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        binutils \
        gnupg2 \
        libc6-dev \
        libcurl4 \
        libedit2 \
        libgcc-9-dev \
        libncurses6 \
        libpython2.7 \
        libsqlite3-0 \
        libstdc++-10-dev \
        libxml2 \
        libz3-dev \
        make \
        pkg-config \
        tzdata \
        zlib1g-dev \
        binaryen \
        wabt \
        gzip \
        brotli && \
    apt-get purge -y --auto-remove ca-certificates curl

RUN swift --version
RUN wasm2wat --version
RUN wasm-opt --version
