build-wasm:
	swift build -c release --triple wasm32-unknown-wasi \
		-Xswiftc -Xfrontend -Xswiftc -disable-availability-checking \
		-Xswiftc -Osize \
		-Xswiftc -whole-module-optimization
	wasm-strip .build/wasm32-unknown-wasi/release/SwiftSoupWasm.wasm

build: build-wasm
	mkdir -p public/assets/wasm/
	cp -r index.html Public/. public/
	wasm-opt -Os .build/wasm32-unknown-wasi/release/SwiftSoupWasm.wasm -o public/assets/wasm/SwiftSoupWasm.wasm

clean:
	rm -rf public/

.PHONY: build-wasm build clean
