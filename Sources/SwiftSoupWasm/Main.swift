import Foundation
import SwiftSoup

@main
struct Main {
    struct ParseResult: Codable {
        var text: String
        var html: String
    }

    struct Output: Codable {
        var error: String
        var outerHtml: String
        var parseResult: [ParseResult]
    }

    static func main() throws {
        let encoder = JSONEncoder()
        do {
            let html = CommandLine.arguments[1]
            let selector = CommandLine.arguments[2]

            let document = try SwiftSoup.parse(html)
            let elements = try document.select(selector)
            let parseResults = try elements.map { (element) -> ParseResult in
                let text = try element.text()
                let outerHtml = try element.outerHtml()
                return ParseResult(text: text, html: outerHtml)
            }
            let output = Output(error: "", outerHtml: try document.outerHtml(), parseResult: parseResults)
            let data = try encoder.encode(output)
            print(String(data: data, encoding: .utf8) ?? "")
        } catch {
            let output = Output(error: error.localizedDescription, outerHtml: "", parseResult: [])
            let data = try encoder.encode(output)
            print(String(data: data, encoding: .utf8) ?? "")
        }
    }
}
