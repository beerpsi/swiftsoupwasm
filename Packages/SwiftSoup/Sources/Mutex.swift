//
//  Mutex.swift
//  SwiftSoup
//
//  Created by xukun on 2022/3/31.
//  Copyright © 2022 Nabil Chatbi. All rights reserved.
//

import Foundation

// Empty out the Mutex implementation as a quick hack, since WASM threads
// are not yet supported.
final class Mutex: NSLocking {
    func lock() {

    }

    func unlock() {

    }
}
