this is basically the [try swiftsoup](https://swiftsoup.herokuapp.com) but it uses wasm instead of a backend API so you get much faster feedback.

## building
```
make build
```
requires swiftwasm installed.
