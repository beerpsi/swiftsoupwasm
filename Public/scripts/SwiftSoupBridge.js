import { WASI } from '../assets/lib/@wasmer/wasi.js';
import { WasmFs } from '../assets/lib/@wasmer/wasmfs.js';

class WASICommand {
  constructor(wasmModule) {
    this.wasmModule = wasmModule;
  }

  static async load(wasmFile) {
    if (WebAssembly.compileStreaming) {
      const wasmModule = await WebAssembly.compileStreaming(fetch(wasmFile));
      return new WASICommand(wasmModule);
    } else {
      const response = await fetch(wasmFile)
      const bytes = await response.arrayBuffer();
      const wasmModule = await WebAssembly.compile(bytes);
      return new WASICommand(wasmModule);
    }
  }

  async exec(args) {
    const wasmFs = new WasmFs();
    let stdout = "";
    let stderr = "";
    const originalWriteSync = wasmFs.fs.writeSync;
    wasmFs.fs.writeSync = (fd, buffer, offset, length, position) => {
      const text = new TextDecoder("utf-8").decode(buffer);
      switch (fd) {
        case 1:
          stdout += text;
          break;
        case 2:
          stderr += text;
          console.error(text);
          break;
      }
      return originalWriteSync(fd, buffer, offset, length, position);
    };

    const wrapWASI = (wasiObject) => {
      // PATCH: @wasmer-js/wasi@0.x forgets to call `refreshMemory` in `clock_res_get`,
      // See also https://github.com/swiftwasm/carton/blob/2c5cf34e0cbb6a4807445ef62a8404bac40314b1/entrypoint/common.js#L135-L145
      const original_clock_res_get = wasiObject.wasiImport["clock_res_get"];

      wasiObject.wasiImport["clock_res_get"] = (clockId, resolution) => {
        wasiObject.refreshMemory();
        return original_clock_res_get(clockId, resolution);
      };
      return wasiObject.wasiImport;
    };
    const wasi = new WASI({
      args: ["main.wasm", ...args],
      bindings: {
        ...WASI.defaultBindings,
        fs: wasmFs.fs,
      },
    });
    const instance = await WebAssembly.instantiate(this.wasmModule, {
      wasi_snapshot_preview1: wrapWASI(wasi),
    });
    wasi.start(instance);
    return { stdout, stderr };
  }
}

export class SwiftSoup {
  constructor() {
    this.command = WASICommand.load("./assets/wasm/SwiftSoupWasm.wasm");
    this.table = document.getElementById("swiftsoup-table");
    this.outerHtml = document.getElementById("swiftsoup-outerHtml");
  }

  async parse(message) {
    console.debug(message);
    const command = await this.command;
    const { stdout } = await command.exec([message.html, message.selector])
    const result = JSON.parse(stdout);
    console.debug(result);

    if (result.error.length > 0) {
      throw new Error(result.error);
    }

    for (let i = 0; i < this.table.rows.length; i++) {
      this.table.deleteRow(i);
    }
    this.outerHtml.value = result.outerHtml;

    const parseResult = result.parseResult;
    for (let i = 0; i < parseResult.length; i++) {
      const model = parseResult[i];
      // Create an empty <tr> element and add it to the 1st position of the table:
      let row = this.table.insertRow(0);
      // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
      let cell1 = row.insertCell(0);
      let h3 = document.createElement("H4");
      h3.innerHTML = model.text;
      cell1.appendChild(h3);

      let pre = document.createElement("pre");
      let code = document.createElement("code");
      pre.className = "pre-scrollable";
      code.textContent = model.html;
      pre.appendChild(code);
      cell1.appendChild(pre);
    }
  }

  reload() {
    const html = document.getElementById("swiftsoup-html").value;
    const selector = document.getElementById("swiftsoup-selector").value;
    this.parse({ html, selector })
  }
}

const swiftSoup = new SwiftSoup();
document.getElementById("swiftsoup-submit").onclick = () => { swiftSoup.reload() };
if (document.readyState === "complete" || document.readyState === "interactive") {
  swiftSoup.reload();
} else {
  document.addEventListener("DOMContentLoaded", () => {
    swiftSoup.reload();
  });
}
